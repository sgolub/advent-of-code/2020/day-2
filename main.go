package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/sgolub/advent-of-code/base/v2/days"
)

type myDay struct {
	data map[string]string
}

func (d myDay) SetData(key, value string) {
	d.data[key] = value
}

// Year will return the AOC Year
func (d myDay) Year() int {
	return 2020
}

// Day will return the day number for this puzzle
func (d myDay) Day() int {
	return 2
}

type passwordLimit struct {
	Min  int
	Max  int
	Char rune
}

func newPasswordLimit(limit string) (passwordLimit, error) {
	splits := strings.Split(limit, " ")
	pLimit := passwordLimit{}
	if len(splits) != 2 {
		return pLimit, fmt.Errorf("invalid limit string: %s", limit)
	}
	if len(splits[1]) != 1 {
		return pLimit, fmt.Errorf("unexpected character: %s", splits[1])
	}
	pLimit.Char = []rune(splits[1])[0]

	ranges := strings.Split(splits[0], "-")
	if len(ranges) != 2 {
		return pLimit, fmt.Errorf("invalid range: %s", splits[0])
	}
	low, err := strconv.Atoi(ranges[0])
	if err != nil {
		return pLimit, fmt.Errorf("invalid integer for low: %s", ranges[0])
	}
	pLimit.Min = low
	high, err := strconv.Atoi(ranges[1])
	if err != nil {
		return pLimit, fmt.Errorf("invalid integer for high: %s", ranges[1])
	}
	pLimit.Max = high

	return pLimit, nil
}

func (l passwordLimit) validate(p string) bool {
	count := 0
	for _, c := range p {
		if c != l.Char {
			continue
		}
		count++
		if count > l.Max {
			return false
		}
	}
	return count >= l.Min
}

func (l passwordLimit) validate2(p string) bool {
	var (
		a int
		b int
	)
	if rune(p[l.Min-1]) == l.Char {
		a = 1
	}
	if rune(p[l.Max-1]) == l.Char {
		b = 1
	}
	return a^b == 1
}

// GetData will load the data and parse it from disk
func (d myDay) GetData(useSampleData bool) map[string][]string {
	result := map[string][]string{}
	data, ok := days.Control().LoadData(d, useSampleData)["passwords"]
	if !ok {
		return result
	}
	for _, limitMap := range data.([]interface{}) {
		for k, v := range limitMap.(map[interface{}]interface{}) {
			key := k.(string)
			if _, ok := result[key]; !ok {
				result[key] = []string{}
			}
			result[key] = append(result[key], v.(string))
		}
	}
	return result
}

// Solution1 is the solution to the first part of the puzzle
func (d myDay) Solution1(useSample bool) string {
	defer days.NewTimer("D2P1")
	validCount := 0
	for l, pList := range d.GetData(useSample) {
		limit, err := newPasswordLimit(l)
		if err != nil {
			fmt.Println(err)
		}
		for _, p := range pList {
			if limit.validate(p) {
				validCount++
			}
		}
	}
	return fmt.Sprintf("%d", validCount)
}

// Solution2 is the solution to the second part of the puzzle
func (d myDay) Solution2(useSample bool) string {
	defer days.NewTimer("D2P2")
	validCount := 0
	for l, pList := range d.GetData(useSample) {
		limit, err := newPasswordLimit(l)
		if err != nil {
			fmt.Println(err)
		}
		for _, p := range pList {
			if limit.validate2(p) {
				validCount++
			}
		}
	}
	return fmt.Sprintf("%d", validCount)
}

// GetDay will return this module's day. Used by the plugin loader.
func GetDay() days.Day {
	return myDay{
		data: map[string]string{},
	}
}
